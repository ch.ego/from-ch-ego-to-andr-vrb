package com.chelnokov.practice.enums;

//роль пользователя сервиса
public enum UserRole {
    ADMIN,
    EMPLOYEE,
    VETERINARIAN,
    SUPER_ADMIN;
}
