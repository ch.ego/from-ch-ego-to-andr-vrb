package com.chelnokov.practice.enums;

public enum Gender {
    MALE, FEMALE;
}
