package com.chelnokov.practice.entity;

/**
 * Вид животного
 */
public class AnimalKind {
    private String kind;//вид
    private String animalClass;//класс
    private String squad;//отряд
    private byte[] pic;//изображение (векторки в ассетах)
}
