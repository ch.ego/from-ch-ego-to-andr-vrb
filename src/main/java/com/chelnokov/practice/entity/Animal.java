package com.chelnokov.practice.entity;

import com.chelnokov.practice.enums.AnimalStatus;
import com.chelnokov.practice.enums.Gender;

import java.util.List;

/**
 * Животное и его характеристики
 */

public class Animal {
    private Long id;
    private String name;
    private Gender gender;//пол животного
    private User user;//привязанный сотрудник, смотритель
    private List<Task> tasks;//задачи, связанные с животным
    private int age;
    private int height;
    private double weight;
    private AnimalKind animalKind;//вид животного
    private String location;//расположение (клетки, вольеры)
    private AnimalStatus status;//состояние животного
    private String featuresOfKeeping;//особенности содержания (опционально)
    private String externalFeatures;//внешние особенности (опционально)
}
