package com.chelnokov.practice.entity;

import com.chelnokov.practice.enums.UserRole;

import java.util.List;

/**
 * Пользователь сервиса, им может быть как администратор сайта, так и сотрудник/смотритель
 */
public class User {
    private Long id;
    private String name;
    private String lastName;
    private String patronymic;
    private UserRole userRole;
    private int age;
    private String login;
    private String password;
    private boolean isActive;
    private List<Report> reports;//отчёты
    private List<Animal> animals;//подопечные животные



}
