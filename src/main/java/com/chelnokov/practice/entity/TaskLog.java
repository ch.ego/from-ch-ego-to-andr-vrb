package com.chelnokov.practice.entity;

import com.chelnokov.practice.enums.RepeatType;

import java.time.LocalDateTime;

/**
 * Карточка с выполненной задачей, которая отправляется в архив выполненных задач
 */
public class TaskLog {
    private Long id;
    private Long taskId;
    private Long userId;
    private TaskType taskType;
    private LocalDateTime expiresDateTime;
    private LocalDateTime completedDateTime;
    private RepeatType repeatType;
    private Animal animal;
    private String note;//заметка, комментарий
}
