package com.chelnokov.practice.enums;

import java.util.Arrays;
import java.util.List;

public enum RepeatType {
    ONCE("Один раз"),
    EVERY_DAY("Каждый день"),
    EVERY_WEEK("Каждую неделю"),
    EVERY_MONTH("Каждый месяц"),
    EVERY_YEAR("Каждый год");

    final String typeName;

    RepeatType(String typeName) {
        this.typeName = typeName;
    }

    public static List<String> getValues(){
        return Arrays.stream(values())
                .map(repeatType -> repeatType.typeName)
                .toList();
    }

    public static RepeatType getByString(String string){
        return Arrays.stream(values())
                .filter(value-> value.typeName.equals(string))
                .findFirst()
                .orElse(ONCE);
    }
}
