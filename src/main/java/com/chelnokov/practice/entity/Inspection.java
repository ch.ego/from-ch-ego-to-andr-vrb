//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.chelnokov.practice.entity;

import java.time.LocalDate;
import java.util.List;

/**
 * Осмотр животных ветеринаром
 */
public class Inspection {
    private Long id;
    private LocalDate date;
    private List<Animal> animalList;//осматриваемые животные
}
