package com.chelnokov.practice.entity;

import com.chelnokov.practice.enums.RepeatType;

import java.time.LocalDateTime;

/**
 * Задача сотрудника-смотрителя, связанная с животным
 */
public class Task {
    private Long id;
    private TaskType taskType; //тип задачи(покормить/почистить вольер),
    // типы задач накапливаются в отдельную таблицу для более удобного составления задач
    private LocalDateTime expiresDateTime;//время истечения
    private boolean completed;//статус, завершена ли задачка
    private RepeatType repeatType;//тип повторения, задача может быть однократной или цикличной
    private Animal animal;//животное, к которому относится задача
    private String note;//заметка, комментарий
}
