package com.chelnokov.practice.entity;

/**
 * Отчёт смотрителя о выполненной задаче
 */
public class Report {
    private Long id;
    private String date;
    private String text;
    private User user;
}
