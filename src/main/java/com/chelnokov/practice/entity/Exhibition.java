package com.chelnokov.practice.entity;

import java.time.LocalDate;
import java.util.List;

/**
 * Выставка/мероприятие, связанное с животными
 */
public class Exhibition {
    private Long id;
    private LocalDate date;//дата выставки
    private List<Animal> animals;//животные-участники выставки
}
